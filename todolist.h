#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

#ifndef _TODOLIST_H
#define _TODOLIST_H


class todolist {


public: // any class can access the methods within

todolist();
void question();
void mainPage(string nickname, string email, string coreID);
void login();
void databaseToVec();
void CallAddToToday(string todayFile, string coreIDS);
void CallAddToFuture(string futureFile, string coreID);
void CallDisplayToday(string todayFile, string coreID);
void CallDisplayFuture(string futureFile, string coreID);
int generateID();

string todooTitle;
string todoo;
int importance;
string FtodooTitle;
string Ftodoo;
int Fimportance;
string StodooTitle;
string Stodoo;
int Simportance;
int Id;
string coreID;
int flag2 = 0;
int navAnswer;
int FnavAnswer;
string nickname;
string email;


void informationRetrieve(string pFailEmail, int flag);
vector<string> readVector;
vector<string> todaysToDoos;
vector<string> futureToDoos;
void signUp();


~todolist();

private: // only current class will have access to the method

string tempEmail;
string tempUsername;
string tempPassword;
string tempNickname;
string username;
string password;

string pFailEmail;
int menuOption;

int flag = 3;
string recoUsername;
string recoPassword;
int attempt = 0;
int appendVar;

protected: // only the current class and subclasses

};



class Bot: public todolist { // bot class inherits from todolist class

public:

  virtual void addtoToday(string todayFile, string coreID); //
  virtual void addtoFuture(string futureFile, string coreID);
  virtual void displayToday(string todayFile, string coreID);
  virtual void displayFuture(string futureFile, string coreID);

};












#endif
