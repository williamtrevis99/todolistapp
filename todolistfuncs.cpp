#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <unistd.h>
#include <cstdlib>

#include "todolist.h"

using namespace std;


todolist::todolist() {

cout << "TODO LISTER\n" << endl;


}


void todolist::question() {

  char response;
  cout << "Do you currently have a TODOO account: " << flush;
  cin >> response;
  if (response == 'y') {
    databaseToVec();
    login();
  } else if (response == 'n') {
    signUp();
  } else {
    cout << "\nInvalid Input. Try Again" << endl;
    login();
  }

}


void todolist::databaseToVec() {

  ifstream file;
  string check;



  file.open ("userinfo.txt");
  if (!file.is_open()) {cout << "Cannot open File" << endl;}

while (file.good()) {
  getline(file,check);
  readVector.push_back(check);
}

file.close();
}


void todolist::signUp() {

  ofstream out;
  out.open("userinfo.txt", ios::app); // opens file in append mode
  cout << "SIGN-UP\n" << endl;
  cout << "Welcome to TODOO" << endl;
  cout << "Please enter the information below to sign up\n" << endl;

  cout << "Email: " << flush;
  cin >> tempEmail;
  out << tempEmail << endl;
  cout << "Username: " << flush;
  cin >> tempUsername;
  out << tempUsername << endl;
  cout << "Password: " << flush;
  cin >> tempPassword;
  out << tempPassword  << endl;
  cout << "Nickname: " << flush;
  cin >> tempNickname;
  out << tempNickname  << endl;
  out << generateID() << endl;
  out.close();
  databaseToVec();
  login();




}

int todolist::generateID() {


  databaseToVec();
  int x;
  for (x = 0 ; x < readVector.size() ; x++) {
    string str = to_string(x);
    if (str == readVector[x]) {

      flag2 = 1;
      cout << "found" << endl;
    }
  }

  if (flag2 == 0) {

    return x;
}
else {
  generateID();
}

}


void todolist::login() {

  system("clear");
  cout << "\nPlease Enter your login details below\n" << endl;
  cout << "Username: " << flush;
  cin >> username;
  cout << "Password: " << flush;
  cin >> password;


  for(int x = 0 ; x < readVector.size() ; x++) {
    if (username == readVector[x] && password == readVector[x+1]) {
      nickname = readVector[x+2];
      email = readVector[x-1];
      coreID = readVector[x+3];
      mainPage(nickname,email,coreID);
      break;
    }
  }

    for(int x = 0 ; x < readVector.size() ; x++) {

       if(username == readVector[x]){
        flag = 1;
      }

      else if (password == readVector[x]) {
        flag = 0;
      }
    }




     if (attempt < 3){
      cout << "\nLogin Information Not Found, Please Try Again" << endl;
      usleep(2000000);
      attempt++;
      login();
      }

    else if (attempt == 3){

      system("clear");

      if (flag == 1) {
        cout << "The password entered is inorrect" << endl;
      }

      else if (flag == 0) {
        cout << "The username entered is inorrect" << endl;
      }

        system("clear");
        cout <<("\nEnter your registered email to retrieve information") << endl;
        cout << (": ");
        cin >> pFailEmail;
        informationRetrieve(pFailEmail, flag);
        attempt++;

      }

}


void todolist::informationRetrieve(string pFailEmail, int flag) {


  for(int x = 0 ; x < readVector.size() ; x++) {
    if (pFailEmail == readVector[x] && flag == 0) {
      recoUsername = readVector[x+1];
      recoPassword = readVector[x+2];
      system("clear");
      cout << "\nHere is a Username Hint.." << endl;
      for (int y = 0 ; y < 3 ; y++) {
        cout << readVector[x+1][y] << flush;
      }
      flag = 3;
      usleep(2000000);
      login();


    }else if (pFailEmail == readVector[x] && flag == 1) {

      recoUsername = readVector[x+1];
      recoPassword = readVector[x+2];
      system("clear");
      cout << "\nHere is a Password Hint.." << endl;
      for (int y = 0 ; y < 3 ; y++) {
        cout << readVector[x+2][y] << flush;

      }
    flag = 3;
    usleep(2000000);
    login();
  }

else if(flag == 3 && pFailEmail == readVector[x]) {

  cout << "\nHere is a Username Hint.." << endl;
  for (int y = 0 ; y < 3 ; y++) {
    cout << readVector[x+1][y] << flush;
  }
  usleep(2000000);

  cout << " " << endl;

  cout << "\nHere is a Password Hint.." << endl;
  for (int y = 0 ; y < 3 ; y++) {
    cout << readVector[x+2][y] << flush;
  }
  usleep(4000000);
  login();

}
}

}







void todolist::mainPage(string nickname, string email, string coreID) {

  string todayFile = coreID + "todaystodoos.txt";
  string futureFile = coreID + "futuretodoos.txt";




  system("clear");

  cout << coreID << endl;
  cout << todayFile << endl;
  cout << futureFile << endl;

  cout << "\nMain Menu\n" << endl;
  cout << "Welcome back " << nickname << " !\n" << endl;
  cout << "1. Today's TODOOS" << endl;  // displays todays todos
  cout << "2. Future TODOOS" << endl; // displays future todoos
  cout << "3. TODO Stack" << endl;
  cout << "4. Add TODOO's" << endl;
  cin >> menuOption;
  system("clear");

  if (menuOption == 1) {
    system("clear");
    CallDisplayToday(todayFile, coreID);

  }

  if (menuOption == 2) {
    system("clear");
    CallDisplayFuture(futureFile, coreID);
  }

  if (menuOption == 4) {
    system("clear");
    cout << "\nWhich folder would you like to append an item to ?" << endl;
    cout << "\n1. Today's TODOOS" << endl;
    cout << "2. Future TODOOS" << endl;
    cin >> appendVar;

    if(appendVar == 1) {

      CallAddToToday(todayFile, coreID);

    }if(appendVar == 2) {

      CallAddToFuture(futureFile, coreID);
    }
  }
}



void todolist::CallAddToToday(string todayFile, string coreID) {

  Bot today;
  today.addtoToday(todayFile, coreID);

}


void todolist::CallAddToFuture(string futureFile, string coreID) {

  Bot future;
  future.addtoFuture(futureFile, coreID);
}

void todolist::CallDisplayToday(string todayFile, string coreID) {

  Bot displayTodo;
  displayTodo.displayToday(todayFile, coreID);
}

void todolist::CallDisplayFuture(string futureFile, string coreID) {

  Bot displayfutureToDo;
  displayfutureToDo.displayFuture(futureFile, coreID);
}




 void Bot::addtoToday(string todayFile, string coreID) {

  ofstream today;
  today.open(todayFile, ios::app); // opens file in append mode
  system("clear");
  cout <<"Add to Todays TODOO's"<<endl;
  cout <<"\nTitle of TODOO:"<<flush;
  cin.ignore();
  getline(cin,todooTitle);
  today << todooTitle << endl;
  cout << "" << endl;
  cout <<"Todoo:"<<flush;
  getline(cin,todoo);
  today << todoo << endl;
  cout << "" << endl;
  cout <<"Relative Importance(1-10):"<<flush;
  cin >> importance;
  today << importance << endl;
  mainPage(nickname, email, coreID);

}

 void Bot::addtoFuture(string futureFile, string coreID) {

  ofstream future;
  future.open(futureFile, ios::app);
  system("clear");
  cout << "Add to Future TODOO's" << endl;
  cout << "\nTitle of TODOO: " << flush;
  cin.ignore();
  getline(cin, FtodooTitle);
  future << FtodooTitle << endl;
  cout << "" << endl;
  cout << "Todoo: " << flush;
  getline(cin, Ftodoo);
  future << Ftodoo << endl;
  cout << "" << endl;
  cout << "Relative Importance(1-10): " << flush;
  cin >> Fimportance;
  future << Fimportance << endl;
  mainPage(nickname, email, coreID);
}



void Bot::displayToday(string todayFile, string coreID) {

  system("clear");

  ifstream addTodayToVec;
  addTodayToVec.open(todayFile);
  string String;

  if (!addTodayToVec.is_open()) {cout << "Cannot open File" << endl;}

  while (addTodayToVec.good()) {
  getline(addTodayToVec,String);
  todaysToDoos.push_back(String);
}

for (int x = 0 ; x < todaysToDoos.size() ; x++) {

  cout << todaysToDoos[x] << endl;
  cout << "" << endl;
}

cout << "\n\n(Return to menu = 1)" << endl;

cin >> navAnswer;

if (navAnswer == 1) {
  system("clear");
  mainPage(nickname, email, coreID);
}


}

void Bot::displayFuture(string futureFile, string coreID) {

  system("clear");

  ifstream addFutureToVec;
  addFutureToVec.open(futureFile);
  string FString;

  if (!addFutureToVec.is_open()) {cout << "Cannot open File" << endl;}

  while (addFutureToVec.good()) {
  getline(addFutureToVec,FString);
  futureToDoos.push_back(FString);
}

for (int x = 0 ; x < futureToDoos.size() ; x++) {

  cout << futureToDoos[x] << endl;
  cout << "" << endl;
}

cout << "\n\n(Return to menu = 1)" << endl;

cin >> FnavAnswer;

if (FnavAnswer == 1) {
  system("clear");
  mainPage(nickname, email, coreID);
}


}











todolist::~todolist() {

cout << "\nQUITTING TODO LISTER" <<endl;

}
